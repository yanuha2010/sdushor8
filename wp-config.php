<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sdushor8');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost:80');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '904bcd3f4eb4852aa1112d028a15ec857a2a7cd1d4fe975aa7dd5b296c4113ab');
define('SECURE_AUTH_KEY',  'c5f585fe92a3cb431cd12003fe94781af6358b00db424f7888427cedf3ba7da8');
define('LOGGED_IN_KEY',    '5dc45bc9b94c29c649ae9922ae024d4c2378ef3ba2b904ee54ddbb504d3b2fa3');
define('NONCE_KEY',        'cf0cdd70fc26e45dd8f933321e87d6088d4b64f9308c4a3dd51e10946afb4a0f');
define('AUTH_SALT',        'baef2622cc30ff109134b641572f608331a1a6e6c4003d3437e92a9dd9f09fa0');
define('SECURE_AUTH_SALT', '26dd43456a9bd0c2eb3fb216c5e145b305ff35b01ab83b62df4fdce1eb978ca7');
define('LOGGED_IN_SALT',   'd7ca8f7ea4ad1d166cdc8d42e67e8da1c51fd815acac2864c4c01f51c57f6f43');
define('NONCE_SALT',       '6032219a43afc492fd14d99d98b3ef56803374bd00046f67c7d3b0dc9f1a06ae');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */
/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','http://example.com');
 *  define('WP_SITEURL','http://example.com');
 *
*/

define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/');
define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/');


/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_TEMP_DIR', ABSPATH . 'tmp/');

