<?php
// function enqueue_styles() {
//     wp_enqueue_style( 'sporturkomplex-style', get_stylesheet_uri());
//     // wp_register_style('font-style', 'http://fonts.googleapis.com/css?family=Open+Sans:400:italic*/');
//     // wp_enqueue_style( 'font-style');
// }


// add_action('wp_enqueue_scripts', 'enqueue_styles');

function load_style_scripts () {
	// css
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
	// js
	wp_enqueue_script( 'jquery-1.10.1.min', get_template_directory_uri() . '/js/jquery-1.10.1.min.js');
	wp_enqueue_script( 'jquery-migrate-1.1.1', get_template_directory_uri() . '/js/jquery-migrate-1.1.1.js');
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js');
	wp_enqueue_script( 'superfish', get_template_directory_uri() . '/js/superfish.js');
	wp_enqueue_script( 'equalheights', get_template_directory_uri() . '/js/jquery.equalheights.js');
	wp_enqueue_script( 'mobilemenu', get_template_directory_uri() . '/js/jquery.mobilemenu.js');
	wp_enqueue_script( 'jquery.easing.1.3', get_template_directory_uri() . '/js/jquery.easing.1.3.js');
	wp_enqueue_script( 'tmStickUp', get_template_directory_uri() . '/js/tmStickUp.js');
	wp_enqueue_script( 'jquery.ui.totop', get_template_directory_uri() . '/js/jquery.ui.totop.js');
	wp_enqueue_script( 'owl.carousel', get_template_directory_uri() . '/js/owl.carousel.js');
	wp_enqueue_script( 'camera', get_template_directory_uri() . '/js/camera.js');
	// wp_enqueue_script( 'jquery.tabs.min', get_template_directory_uri() . '/js/jquery.tabs.min.js');

  wp_register_script('html5-shim', 'http://html5shim.googlecode.com/svn/trunk/html5.js');
  // wp_enqueue_script('html5-shim');
}

// download_styles_and_scripts
add_action('wp_enqueue_scripts', 'load_style_scripts');
// remove_filter('the_content', 'wpautop');



/**
 * Register two widget areas.
 *
 * @since Twenty Thirteen 1.0
 */
function victoriya_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Виджет формы обратной связи', 'victoriya' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Здесть выводится виджет с формой обратной связи', 'victoriya' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title"><span>',
		'after_title'   => '</span></h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Виджет котактов футера', 'victoriya' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Здесь выводится виджет с конактами в футере главной страницы', 'twentythirteen' ),
		'before_widget' => '<div id="%1$s" class="widget row">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'victoriya_widgets_init' );

// add-miniatures
add_theme_support( 'post-thumbnails' );

// add_menu
register_nav_menu( 'menu', 'Меню' );



// slider

add_action('init', 'slider');

function slider() {
	register_post_type('slider', array (
		 'public' => true
		,'menu_icon' => 'dashicons-format-gallery'
		,'supports' => array('title', 'thumbnail', 'editor', 'excerpt')
		,'labels' => array(
					 'name' => 'Слайдер'
					,'all_items' => 'Все слайды'
					,'singular_name' => 'Слайд'
					,'add_new' => 'Добавить слайд'
					,'add_new_item' => 'Добавить новый слайд'
		)
	)); 
}


add_action('init', 'services');



function services() {
	register_post_type('services', array (
		 'public' => true
		,'menu_icon' => 'dashicons-megaphone'
		,'supports' => array('title', 'thumbnail', 'editor', 'excerpt')
		,'labels' => array(
					 'name' => 'Услуги на главной'
					,'all_items' => 'Все услуги'
					,'singular_name' => 'Услугу'
					,'add_new' => 'Добавить услугу'
					,'add_new_item' => 'Добавить новую услугу'
		)
	)); 
}



function my_pagenavi() {
	global $wp_query;

	$big = 999999999; // уникальное число для замены

	$args = array(
		'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) )
		,'format' => ''
		,'current' => max( 1, get_query_var('paged') )
		,'total' => $wp_query->max_num_pages
	);

	$result = paginate_links( $args );

	// удаляем добавку к пагинации для первой страницы
	$result = str_replace( '/page/1"', '"', $result );

	echo $result;
}

// Теперь, где нужно вывести пагинацию используем 
// my_pagenavi();

?>