<?php get_header(); ?>

<section class="content">
	<div class="container">
		<div class="row">
			<?php if (have_posts()): while (have_posts()): the_post(); ?>

			<h2><span><?php wp_title("", true); ?></span></h2>
			<!-- post -->
			<div class="blog1 clearfix">
				<?php the_content(); ?>
			</div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>