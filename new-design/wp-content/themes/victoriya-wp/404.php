<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Victoriya
 * @since Victoriya 1.0
 */

get_header(); ?>

	<section class="content">
		<div class="container">
			<div class="row">
				<h2><span><?php wp_title("", true); ?></span></h2>

				<div class="page-wrapper">
					<div class="page-content">
						<h2 class="page-title"><?php _e( '404', 'victoriya' ); ?></h2>

					</div><!-- .page-content -->
				</div><!-- .page-wrapper -->
			</div>
		</div>
	</section>

<?php get_footer(); ?>