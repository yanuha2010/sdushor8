<?php
/*
Template Name: Главная
*/
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
?>
<?php get_header(); ?>

<?php $slider = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => -1, 'order' => 'ASC' ) );?>

<?php if ( $slider->have_posts() ) : ?>
  <div class="slider_wrapper ">
    <div id="camera_wrap" class="">

      <?php while ( $slider->have_posts() ) : $slider->the_post(); ?>

      <div data-src="<?php
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
        echo $thumb_url[0];
      ?>">
        <div class="caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="grid_6 preffix_6">
                <h2><?php the_content(); ?><p class="title"><?php the_excerpt(); ?></p></h2>
                <?php
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>

<?php endwhile; ?>

    </div>
    <img src="<?php bloginfo('template_url'); ?>/images/top_angle_white.png" alt="" class="angle top__angle">
  </div>


<?php else: ?>
    <h2>Место для слайдера</h2>
<?php endif; ?>
<?php wp_reset_query(); ?>

<!--=====================
          Content
======================-->
<section class="content">
  <div class="container">
    <div class="row">
      <div class="grid_12">
        <h2 class="head1"><span>Новости</span></h2>

        <div id="owl">
          <?php $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
          $args = array(
            'posts_per_page' => get_option('posts_per_page'), // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
            'paged'          => $current_page // текущая страница
          );
          query_posts( $args );
           
          $wp_query->is_archive = true;
          $wp_query->is_home = false; ?>
          <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <!-- post -->
          <div class="item">
            <a href="<?php the_permalink(); ?>" class="new">
              <time class="abs_time" datetime=""><?php the_time('j F Y'); ?></time>
              <div class="news_top">
                <div>
                  <div class="text1"><?php the_title(); ?></div>
                  <?php the_excerpt(); ?>
                </div>
              </div>
              <div class="thumbnail-index-cr">
                <?php 
                  if( has_post_thumbnail() ) {
                    the_post_thumbnail( array(220, 200), 'class=imgstyle' );
                  } else {
                    echo '<img src="'.get_bloginfo("template_url").'/images/camera-icon-md.png" />';
                  }
                ?>
              </div>
            </a>
          </div>
          <?php endwhile; ?>
            <!-- no posts found -->
          <?php endif; ?>

        </div>
      </div>

      
      <div class="grid_12">
        <h2><span>Услуги</span></h2>
      </div>

  <?php $services = new WP_Query( array( 'post_type' => 'services', 'posts_per_page' => -1, 'order' => 'ASC' ) );?>

  <?php if ( $services->have_posts() ) : ?>

  <?php while ( $services->have_posts() ) : $services->the_post(); ?>
      <div class="grid_4">
        <div class="block1">
          <a href="<?php the_permalink(); ?>" class="block1_link">
          <?php 
            if( has_post_thumbnail() ) {
              the_post_thumbnail( 'medium', 'class=imgstyle' );
            } else {
              echo '<img src="'.get_bloginfo("template_url").'/images/camera-icon-md.png" />';
            }
          ?>
          <span class="block1_capt"><span>Подробнее</span></span></a>
          <div class="text1"><?php the_title(); ?></div>

          <?php the_excerpt(); ?>
        </div>
      </div>
  <?php endwhile; ?>

  <?php else: ?>
      <h2>Место для услуг</h2>
  <?php endif; ?>

  <?php wp_reset_query(); ?>

</section>
<section class="contacts">
  <img src="<?php bloginfo('template_url'); ?>/images/top_angle_red.png" class="angle bot__angle" alt="">
  <div class="container">
    <div class="row">
      <?php dynamic_sidebar('sidebar-2'); ?> 
    </div>
  </div>
</section>
<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=LU4p05mvTxWpkPW4Q3Gp7NggYUpfQ9Y4&width=100%&height=350"></script>

<?php get_footer(); ?>
