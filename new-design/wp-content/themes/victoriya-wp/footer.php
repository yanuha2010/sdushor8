
		<!--==============================
              footer
=================================-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_12">
         <div class="copyright">Copyright  &copy; <span id="copyright-year"></span> | <a <?php echo esc_url( home_url( '/' ) ); ?> title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a>
          </div>
      </div>

    </div>
  </div>
</footer>

</body>
</html>

	<?php wp_footer(); ?>
</body>
</html>