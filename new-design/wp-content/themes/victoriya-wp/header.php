<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Victoriya
 * @since Victoriya 1.0
 */
?><!DOCTYPE html>

<html lang="en">
<head>
<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<meta charset="UTF-8">
<meta name = "format-detection" content = "telephone=no" />
<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico"/>

<?php wp_head(); ?>

<script>
 $(document).ready(function(){
  jQuery('#camera_wrap').camera({
      loader: false,
      pagination: true ,
      minHeight: '200',
      thumbnails: false,
      height: '33.66492146596859%',
      caption: true,
      navigation: false,
      fx: 'mosaic'
    });
  /* carousel */
   $("#owl").owlCarousel({
        items : 4, //10 items above 1000px browser width
        itemsDesktop : [995,4], //5 items between 1000px and 901px
        itemsDesktopSmall : [767, 2], // betweem 900px and 601px
        itemsTablet: [700, 2], //2 items between 600 and 0
        itemsMobile : [479, 1], // itemsMobile disabled - inherit from itemsTablet option
        navigation : false,
        pagination : true
    });

  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});
  }); 
</script>

<!--[if (gt IE 9)|!(IE)]><!
<script src="<?php bloginfo('template_url'); ?>/js/jquery.mobile.customized.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->

</head>
<body <?php body_class(); ?>>
<!--==============================
              header
=================================-->
<header>

<!--==============================
            Stuck menu
=================================-->
<section id="stuck_container">
  <div class="container container-nav">
    <h1>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
        <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="logo">
      </a>
    </h1>
    <div class="navigation">
      <nav>
        <?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false, 'menu_class' => 'sf-menu', )); ?>
      </nav>

      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </section>
</header>