<?php get_header(); ?>

<section class="content">
	<div class="container">
		<div class="clearfix">
			<h2><span><?php wp_title("", true); ?></span></h2>
			<?php if (have_posts()): while (have_posts()): the_post(); ?>
					<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>