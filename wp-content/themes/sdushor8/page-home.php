<?php
/*
Template Name: Главная
*/
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );
?>
<?php get_header(); ?>

<?php $slider = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => -1, 'order' => 'ASC' ) );?>

<?php if ( $slider->have_posts() ) : ?>
  <script>
   $(document).ready(function(){
    jQuery('#camera_wrap').camera({
        loader: false,
        pagination: true ,
        minHeight: '200',
        thumbnails: false,
        height: '33.66492146596859%',
        caption: true,
        navigation: false,
        fx: 'mosaic'
      });
    /* carousel */
     $("#owl").owlCarousel({
          items : 4, //10 items above 1000px browser width
          itemsDesktop : [995,4], //5 items between 1000px and 901px
          itemsDesktopSmall : [767, 2], // betweem 900px and 601px
          itemsTablet: [700, 2], //2 items between 600 and 0
          itemsMobile : [479, 1], // itemsMobile disabled - inherit from itemsTablet option
          navigation : false,
          pagination : true
      });

    $().UItoTop({ easingType: 'easeOutQuart' });
    $('#stuck_container').tmStickUp({});
    }); 
  </script>
  <div class="slider_wrapper ">
    <div id="camera_wrap" class="">

      <?php while ( $slider->have_posts() ) : $slider->the_post(); ?>

      <div data-src="<?php
        $thumb_id = get_post_thumbnail_id();
        $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
        echo $thumb_url[0];
      ?>">
<!--         <div class="caption fadeIn">
          <div class="container">
            <div class="row">
              <div class="grid_6 preffix_6">
                <h2><?php the_content(); ?><p class="title"><?php the_excerpt(); ?></p></h2>
                <?php
                ?>
              </div>
            </div>
          </div>
        </div> -->
      </div>

<?php endwhile; ?>

    </div>
    <img src="<?php bloginfo('template_url'); ?>/images/top_angle_white.png" alt="" class="angle top__angle">
  </div>


<?php else: ?>
    <h2>Место для слайдера</h2>
<?php endif; ?>
<?php wp_reset_query(); ?>

<!--=====================
          Content
======================-->
<section class="content">
  <div class="container">
    <div class="clearfix">
      <h2 class="head1"><span>Новости</span></h2>

      <div id="owl">
        <?php $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
        $args = array(
          'posts_per_page' => get_option('posts_per_page'), // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
          'paged'          => $current_page // текущая страница
        );
        query_posts( $args );
         
        $wp_query->is_archive = true;
        $wp_query->is_home = false; ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- post -->
        <div class="item">
          <a href="<?php the_permalink(); ?>" class="new">
            <time class="abs_time" datetime=""><?php the_time('j F Y'); ?></time>
            <div class="thumbnail-index-cr">
              <?php 
                if( has_post_thumbnail() ) {
                  the_post_thumbnail( array(220, 200), 'class=imgstyle' );
                } else {
                  echo '<img src="'.get_bloginfo("template_url").'/images/camera-icon-md.png" />';
                }
              ?>
            </div>
            <div class="news_top">
              <div>
                <div class="text1"><?php the_title(); ?></div>
                <?php echo trim_characters(90, '...'); ?>
              </div>
            </div>
          </a>
        </div>
        <?php endwhile; ?>
          <!-- no posts found -->
        <?php endif; ?>

      </div>
    </div>
  </div>
  <?php wp_reset_query(); ?>

</section>
<section class="contacts">
<!--   <img src="<?php bloginfo('template_url'); ?>/images/top_angle_red.png" class="angle bot__angle" alt=""> -->
  <div class="container">
    <?php dynamic_sidebar('sidebar-2'); ?> 
  </div>
</section>
<!-- <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=LU4p05mvTxWpkPW4Q3Gp7NggYUpfQ9Y4&width=100%&height=350"></script> -->
<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=xX3fRcLlNYmNd57_XoWAuIgaP1cQ5doH&amp;width=100%&amp;height=350&amp;lang=ru_RU"></script>

<?php get_footer(); ?>
