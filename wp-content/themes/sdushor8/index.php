<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

  <section class="content">
    <div class="container">
      <div class="clearfix">

        <h2><span><?php wp_title("", true); ?></span></h2>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- post -->
        <div class="blog1 clearfix">
          <div class="rel img_inner fleft">
          <?php 
            if( has_post_thumbnail() ) {
              the_post_thumbnail( 'medium', 'class=imgstyle' );
            } else {
              echo '<img src="'.get_bloginfo("template_url").'/images/camera-icon-md.png" />';
            }
            ?>
            <time class="abs_time" datetime=""><?php the_time('j F Y'); ?></time>
          </div>
          <div class="extra_wrapper">
            <div>
              <div class="text1"><?php the_title(); ?></div>
              <?php the_excerpt(); ?>
              <a href="<?php the_permalink(); ?>" class="btn">Подробнее</a>
            </div>
          </div>
        </div>

        <?php endwhile; ?>
        <div class="pagination">
          <?php my_pagenavi(); ?>
        </div>
        <!-- post navigation -->
        <?php else: ?>
        <!-- no posts found -->
        <?php endif; ?>
      </div>

    </div>
  </section>

<?php get_footer(); ?>