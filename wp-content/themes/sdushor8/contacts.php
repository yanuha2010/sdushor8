<?php
/*
Template Name: Контакты
*/
?>

<?php get_header(); ?>

<section class="content">
	<div class="container">
		<div class="row">
			<?php if (have_posts()): while (have_posts()): the_post(); ?>

				<div class="grid_8">
					<h2><span><?php wp_title("", true); ?></span></h2>
					<div class="map">
					<figure class="figure-map"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=xX3fRcLlNYmNd57_XoWAuIgaP1cQ5doH&amp;width=100%&amp;height=350&amp;lang=ru_RU"></script></figure>
						<?php the_content(); ?>
					</div>
				</div>

				<div class="grid_4">
					<?php dynamic_sidebar('sidebar-1'); ?> 
				</div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>