<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Victoriya
 * @since Victoriya 1.0
 */
?><!DOCTYPE html>

<html lang="en">
<head>
<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
<meta charset="UTF-8">
<meta name = "format-detection" content = "telephone=no" />
<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/images/favicon-16x16.png" sizes="16x16" />

<?php wp_head(); ?>

<!--[if (gt IE 9)|!(IE)]><!
<script src="<?php bloginfo('template_url'); ?>/js/jquery.mobile.customized.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->

</head>
<body <?php body_class(); ?>>
<!--==============================
              header
=================================-->
<header>

  <!--==============================
              Stuck menu
  =================================-->
  <section id="stuck_container">
    <div class="container container-nav">
      <h1 class="logo__wr">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
          <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="logo">
        </a>
      </h1>
      <div class="navigation">
        <nav class="nav-left">
          <?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false, 'menu_class' => 'sf-menu', 'menu' => 'header-menu-left')); ?>
        </nav>


        <nav class="nav-right">
          <?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false, 'menu_class' => 'sf-menu', 'menu' => 'header-menu-right')); ?>
        </nav>

        <nav class="nav-mobile">
          <?php wp_nav_menu( array( 'theme_location' => 'menu', 'container' => false, 'menu_class' => 'mobile-menu', 'menu' => 'header-menu-mobile')); ?>
        </nav>

        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
  </section>
</header>