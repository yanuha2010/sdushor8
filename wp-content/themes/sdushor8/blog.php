<?php
/*
 * Template name: Блог
 */

get_header(); ?>

	<section class="content">
		<div class="container">
			<div class="clearfix">

			<?php $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
			$args = array(
				'posts_per_page' => get_option('posts_per_page'), // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
				'paged'          => $current_page // текущая страница
			);
			query_posts( $args );
			 
			$wp_query->is_archive = true;
			$wp_query->is_home = false;

			while(have_posts()): the_post();
				?>
					<div class="blog1 clearfix">
						<div class="rel img_inner fleft">
							<?php the_post_thumbnail( 'medium', 'class=imgstyle' ); ?>
							<time class="abs_time" datetime=""><?php the_time('j F Y'); ?></time>
						</div>
						<div class="extra_wrapper">
							<div>
								<div class="text1"><a href=" <?php the_permalink(); ?>"><?php the_title(); ?></a></div>
								<?php the_excerpt(); ?>
								<a href="<?php the_permalink(); ?>" class="btn">Подробнее</a>
							</div>
						</div>
					</div>

			<?php endwhile;
				if( function_exists('wp_pagenavi') ) wp_pagenavi(); // функция постраничной навигации
			?>

				</div>
		</div>
	</section>

<?php get_footer(); ?>